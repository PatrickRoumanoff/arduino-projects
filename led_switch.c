
int green1 = 9;
int red1 = 8;
int jaune1 = 6;
int green = 12;
int red = 11;
int jaune = 10;
int button = 7;
int val = 0; 

void setup() {
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(jaune, OUTPUT);
  pinMode(green1, OUTPUT);
  pinMode(red1, OUTPUT);
  pinMode(jaune1, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  digitalWrite(green, LOW);
  digitalWrite(red, LOW);
  digitalWrite(jaune, LOW);
  digitalWrite(green1, LOW);
  digitalWrite(red1, LOW);
  digitalWrite(jaune1, LOW);
  Serial.begin(9600);
}

void loop() {
  val = digitalRead(button);
  digitalWrite(green, val);
  delay(250); 
  val = digitalRead(button);
  digitalWrite(green, LOW); 
  digitalWrite(green1, val);
  delay(250); 
  val = digitalRead(button);
  digitalWrite(green1, LOW); 
  digitalWrite(red, val);
  delay(250);  
  val = digitalRead(button);
  digitalWrite(red, LOW);
  digitalWrite(red1, val);
  delay(250);  
  val = digitalRead(button);
  digitalWrite(red1, LOW);
  digitalWrite(jaune, val);
  delay(250);  
  val = digitalRead(button);
  digitalWrite(jaune, LOW);
  digitalWrite(jaune1, val);
  delay(250);  
  val = digitalRead(button);
  digitalWrite(jaune1, LOW);
  digitalWrite(jaune, val);
  delay(250);  
  val = digitalRead(button);
  digitalWrite(jaune, LOW);
  digitalWrite(red1, val);
  delay(250);  
  val = digitalRead(button);
  digitalWrite(red1, LOW);
  digitalWrite(red, val);
  delay(250);  
  val = digitalRead(button);
  digitalWrite(red, LOW);
  digitalWrite(green1, val);
  delay(250); 
  digitalWrite(green1, LOW); 
}

